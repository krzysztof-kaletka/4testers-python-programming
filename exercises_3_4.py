message_requires = "Zwierze wymaga szczepienia"
message_not_requires = "Zwierze nie wymaga szczepienia"
message_negative_old = "Wiek zwierzęcia nie może być ujemny."
message_accept_kind_of_animal = "Szczepimy tylko koty i psy."


def requires_vaccination(kind_of_animal, age_of_animal):
    if age_of_animal < 1:
        raise ValueError(message_negative_old)
    if kind_of_animal not in ["kot", "pies"]:
        raise ValueError(message_accept_kind_of_animal)

    if age_of_animal == 1:
        return message_requires
    elif kind_of_animal == "kot" and age_of_animal % 3 == 0:
        return message_requires
    elif kind_of_animal == "pies" and age_of_animal % 2 == 0:
        return message_requires
    else:
        return message_not_requires


try:
    print(requires_vaccination("kot", 1))
    print(requires_vaccination("pies", 1))
    print(requires_vaccination("kot", 3))
    print(requires_vaccination("pies", 4))
    print(requires_vaccination("kot", 2))
    print(requires_vaccination("pies", 3))
    print(requires_vaccination("ryba", 1))
    print(requires_vaccination("kot", -1))
except ValueError as e:
    print("Błąd:", e)
