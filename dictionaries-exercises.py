pet = {
    "name": "Tofik",
    "kind": "dog",
    "age": 7,
    "weight": 44,
    "is_male": True,
    "favourite_food": ["fish", "chicken", "salat"]
}

print(pet["weight"])
pet["weight"] = 47
pet["likes_swimming"] = True
del pet["is_male"]
pet["favourite_food"].append("apple")
print(pet)


def prepare_user_payload(email, phone, city, street):
    return {
        'contact': {
            'email': email,
            'phone': phone
        },
        'address': {
            'city': city,
            'street': street
        }
    }


payload = prepare_user_payload("k.qaletka@gmail.com", 888777666, "Kalisz", "Krakowska 33/2")
print(payload)
