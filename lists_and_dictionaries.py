# Lists
shopping_list = ['oranges', 'water', 'chicken', 'potatoes', 'washing liquid']

print(shopping_list[0])
print(shopping_list[1])
print(shopping_list[-1])

shopping_list.append('lemons')
print(shopping_list[-1])
print(shopping_list)

# liczba elementów
number_of_items_to_buy = len(shopping_list)
print(number_of_items_to_buy)

# wybór zakresu
first_three_shopping_items = shopping_list[0:3]
print(first_three_shopping_items)

# Dictionaries
animal = {
    "name": "Tofik",
    "kind": "dog",
    "age": 7,
    "male": True
}

dog_name = animal["name"]
print("Dog name:", dog_name)

dog_age = animal["age"]
print("Dog age:", dog_age)

animal["age"] = 10
print(animal)

animal["owner"] = "Max"
print(animal)
