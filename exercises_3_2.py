def vallue_of_temperatures(list_temperatures):
    return sum(list_temperatures) / len(list_temperatures)


def average_temperatures_of_months(month_one, month_two):
    return (month_one + month_two) / 2


january = [-4, 1.0, -7, 2]
february = [-13, -9, -3, 3]

print(vallue_of_temperatures(january))
print(vallue_of_temperatures(february))
print(average_temperatures_of_months(vallue_of_temperatures(january), vallue_of_temperatures(february)))
