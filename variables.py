name = 'Tomek'
age = 30
pet_amount = 0
has_driving_license = True
friendship_years = 20.5

friendship_info = f"Name: {name}\nAge: {age}\nPet amount: {pet_amount}\nHas driving license: {has_driving_license}\nFriendship years: {friendship_years}"
print(friendship_info)
