def get_average_grade(list_of_grades):
    return sum(list_of_grades) / len(list_of_grades)


grades = [5, 4, 3, 3, 1, 5]
print(get_average_grade(grades))


def checkTemp(temp_in_celsius, pressure_in_hp):
    if temp_in_celsius == 0 and pressure_in_hp == 1013:
        return True
    else:
        return False


def calculate_grade_for_test_score(points):
    if points >= 90:
        return 5
    elif points >= 75:
        return 4
    elif points >= 50:
        return 3
    else:
        return 2


if __name__ == '__main__':
    print(calculate_grade_for_test_score(105))
    print(calculate_grade_for_test_score(95))
    print(calculate_grade_for_test_score(76))
    print(calculate_grade_for_test_score(52))
    print(calculate_grade_for_test_score(22))
   