emails = ['a@example.com', 'b@example.com']
# długość listy
print(len(emails))
# pierwszy element listy
print(emails[0])
# ostatni element listy
last_value_index = len(emails) - 1
print(emails[-1])
# dodanie nowego email 'e@example.com'
emails.append('c@example.com')
print(emails)


def generate_adres_email(name, lastname):
    email = f"{name.lower()}.{lastname.lower()}@4tester.pl"
    return email


print(generate_adres_email("Adam", "Kowalski"))


def list_of_degres(student_degres):
    return sum(student_degres) / len(student_degres)


degre = [5, 5, 5, 3, 4, 5]
print(list_of_degres(degre))


