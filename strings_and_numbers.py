### Strings ###
first_name = "Krzysztof"
last_name = "Kaletka"
email = "k.qaletka@gmail.com"

my_bio = f"Hi, I'm {first_name}. Surname {last_name}. E-mail: {email}."
print(my_bio)


def format_text(name, age, city):
    formatted_text = f"Hello, my name is {name.capitalize()}" \
                     f"I'm {age} years old and I live in {city.capitalize()}."
    return formatted_text


name = "Krzysztof"
age = 30
city = "PLESZEW"
formatted_message = format_text(name, age, city)
print(formatted_message)

### Algebra ###
circle_radius = 4
area_of_a_circle_with_radius = 3.14 * circle_radius ** 2
circumference_of_a_circle_with_radius = 2 * 3.14 * circle_radius

print(f"Area of a circle with radius {circle_radius}:", area_of_a_circle_with_radius)
print(f"Circumference of a circle with radius {circle_radius}:", circumference_of_a_circle_with_radius)
