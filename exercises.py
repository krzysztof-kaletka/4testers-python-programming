def square_of_the_number(number):
    return number * number


def calculate_cuboid_volume(a, b, c):
    return a * b * c


def convert_celsius_to_farenheit(temp_in_celsius):
    return temp_in_celsius * 9 / 5 + 32


def welcome_sentence(name, city):
    return f"{name}, {city}"


if __name__ == '__main__':
    print(square_of_the_number(10))
    print(calculate_cuboid_volume(3, 5, 7))
    print(convert_celsius_to_farenheit(20))
    print(welcome_sentence("Krzysztof", "Poznań"))
