movies = ["Rocky 1", "Rocky 2", "Rocky 3", "Rocky 4", "Rocky 5"]

print(len(movies))
print(movies[0])
last_movies_index = len(movies) - 1
print(movies[-1])

movies.append("Her")
movies.insert(2, "Interstellar")
print(movies)


def get_thre_highest_numbers_in_the_list(list_of_number):
    # sorted_list = sorted(list_of_number)
    # return sorted_list[-3:]
    sorted_list = sorted(list_of_number, reverse=True)
    return sorted_list[0:3]


print(get_thre_highest_numbers_in_the_list([-9, 1, 8, 44, 5, 332]))
